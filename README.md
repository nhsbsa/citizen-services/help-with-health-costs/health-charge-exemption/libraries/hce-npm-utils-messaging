# health-charge-exemption-npm-utils-messaging

## Description

This npm helper library is used for anything common relating to messaging (i.e. sqs/ sns).

## Installation
```
npm i @nhsbsa/health-charge-exemption-npm-utils-messaging
```