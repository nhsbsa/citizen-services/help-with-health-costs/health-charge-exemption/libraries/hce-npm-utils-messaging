export interface CitizenUpdateSqsBody {
  citizenId: string;
  channel: ChannelSqs;
}

export enum ChannelSqs {
  HRT_ONLINE = "HRT_ONLINE",
  BLC_ONLINE = "BLC_ONLINE",
  HRT_STAFF = "HRT_STAFF",
  LIS_STAFF = "LIS_STAFF",
  PHARMACY = "PHARMACY",
  BATCH = "BATCH",
}
