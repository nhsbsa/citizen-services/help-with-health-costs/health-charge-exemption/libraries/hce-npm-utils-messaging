export interface LowIncomeSchemeSqsBody {
  excessIncome?: number;
  partnerCitizenId?: string;
}

export interface CertificateUpdateSqsBody {
  citizenId: string;
  certificate: {
    id: string;
    type: CertificateTypeSqs;
    status: CertificateStatusSqs;
    reference: string;
    startDate: string;
    endDate: string;
    applicationDate: string;
  };
  lowIncomeScheme?: LowIncomeSchemeSqsBody;
}

export enum CertificateStatusSqs {
  ACTIVE = "ACTIVE",
  PENDING = "PENDING",
  CANCELLED = "CANCELLED",
}

export enum CertificateTypeSqs {
  HRT_PPC = "HRT_PPC",
  LIS_HC2 = "LIS_HC2",
  LIS_HC3 = "LIS_HC3",
}
