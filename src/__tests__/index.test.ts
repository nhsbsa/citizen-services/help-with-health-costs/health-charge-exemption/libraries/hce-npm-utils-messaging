import * as indexFile from "../index";

describe("index file exports", () => {
  test.each(Object.keys(indexFile))("should export %s", (exported) => {
    expect(indexFile[exported]).toBeDefined();
  });
});
