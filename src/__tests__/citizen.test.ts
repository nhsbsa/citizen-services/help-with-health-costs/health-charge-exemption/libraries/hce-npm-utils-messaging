import { CitizenUpdateSqsBody, ChannelSqs } from "../citizen";

test("CitizenUpdateSqsBody interface", () => {
  // given
  const body: CitizenUpdateSqsBody = {
    citizenId: "8e3fc01c-368c-4636-8d5a-571375e52bec",
    channel: ChannelSqs.LIS_STAFF,
  };

  // when / then
  expect(body.citizenId).toEqual("8e3fc01c-368c-4636-8d5a-571375e52bec");
  expect(body.channel).toEqual(ChannelSqs.LIS_STAFF);
});

test("should have HRT_ONLINE, BLC_ONLINE, HRT_STAFF, LIS_STAFF, PHARMACY, and BATCH channels", () => {
  expect(ChannelSqs).toEqual({
    HRT_ONLINE: "HRT_ONLINE",
    BLC_ONLINE: "BLC_ONLINE",
    HRT_STAFF: "HRT_STAFF",
    LIS_STAFF: "LIS_STAFF",
    PHARMACY: "PHARMACY",
    BATCH: "BATCH",
  });
});
