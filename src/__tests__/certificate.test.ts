import {
  CertificateUpdateSqsBody,
  LowIncomeSchemeSqsBody,
  CertificateStatusSqs,
  CertificateTypeSqs,
} from "../certificate";

test("LowIncomeSchemeSqsBody interface", () => {
  // given /when
  const LowIncomeSchemeSqsBody: LowIncomeSchemeSqsBody = {
    excessIncome: 1000,
    partnerCitizenId: "a3de1e66-26b0-4e22-bb62-bdff89be1490",
  };

  // then
  expect(LowIncomeSchemeSqsBody.excessIncome).toBe(1000);
  expect(LowIncomeSchemeSqsBody.partnerCitizenId).toBe(
    "a3de1e66-26b0-4e22-bb62-bdff89be1490",
  );
});

test("CertificateUpdateSqsBody interface", () => {
  // given /when
  const certificateUpdateSqsBody: CertificateUpdateSqsBody = {
    citizenId: "73c088aa-d6d1-4c58-8c2d-bbf361e020b3",
    certificate: {
      id: "61777c0c-5f83-49e7-ad6d-7eac9f708e19",
      type: CertificateTypeSqs.LIS_HC3,
      status: CertificateStatusSqs.ACTIVE,
      reference: "12345678",
      startDate: "2022-09-14",
      endDate: "2023-09-13",
      applicationDate: "2022-09-01",
    },
    lowIncomeScheme: {
      excessIncome: 1000,
      partnerCitizenId: "2f096e53-286a-442c-8ecc-144c518af1fd",
    },
  };

  // then
  expect(certificateUpdateSqsBody.citizenId).toBe(
    "73c088aa-d6d1-4c58-8c2d-bbf361e020b3",
  );
  expect(certificateUpdateSqsBody.certificate.id).toBe(
    "61777c0c-5f83-49e7-ad6d-7eac9f708e19",
  );
  expect(certificateUpdateSqsBody.certificate.type).toBe(
    CertificateTypeSqs.LIS_HC3,
  );
  expect(certificateUpdateSqsBody.certificate.status).toBe(
    CertificateStatusSqs.ACTIVE,
  );
  expect(certificateUpdateSqsBody.certificate.reference).toBe("12345678");
  expect(certificateUpdateSqsBody.certificate.startDate).toBe("2022-09-14");
  expect(certificateUpdateSqsBody.certificate.endDate).toBe("2023-09-13");
  expect(certificateUpdateSqsBody.certificate.applicationDate).toBe(
    "2022-09-01",
  );
  expect(certificateUpdateSqsBody.lowIncomeScheme?.excessIncome).toBe(1000);
  expect(certificateUpdateSqsBody.lowIncomeScheme?.partnerCitizenId).toBe(
    "2f096e53-286a-442c-8ecc-144c518af1fd",
  );
});

test("should have ACTIVE, PENDING, and CANCELLED statuses", () => {
  expect(CertificateStatusSqs).toEqual({
    ACTIVE: "ACTIVE",
    PENDING: "PENDING",
    CANCELLED: "CANCELLED",
  });
});

test("should have HRT_PPC, LIS_HC2, and LIS_HC3 types", () => {
  expect(CertificateTypeSqs).toEqual({
    HRT_PPC: "HRT_PPC",
    LIS_HC2: "LIS_HC2",
    LIS_HC3: "LIS_HC3",
  });
});
